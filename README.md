Loblaw Usecase- Registration-tdd
Description Of Use-Case Implementation –


Use Spring JPA- “@entity” objects to define the table structure and JPARepository/CrudRepository  utilities for all DB operations , defined some custom methods in our JPARepository/CrudRepository object but no single direct SQL queries in code.


Use standard Spring-Cloud-Connector Beans and utilities in @configuration class to connect to DB instances in PCF (Example – MYSQL DB)  , instead of using custom static connection parameters wherever we connect to any cloud marketplace services .
https://cloud.spring.io/spring-cloud-connectors/spring-cloud-spring-service-connector.html


Use standard Payload validation using “javax.validation” with @valid annotations (along with @notnull, @Lenghth etc to auto-validate all the incoming JSON Payload at the beginning , and generate custom validation messages dynamically .


Built exception handing framework , using spring’s standard annotations - @ControllerAdvice and @ExceptionHandler etc , So that all major predefined Exceptions can have single point of exit from application perspective.


The rest URI defined for create and retrieve operations are as below


Create -
HTTP POST – https://{host+domain name}/users/registration
Content-Type – application/json
Retrieve -
HTTP GET - https://{host+domain name}/users/retrieval
Parameters Passed- Header


Junit – Unit test cases, are using Mockito , to mock the dependent objects w.r.t any certain method. Unit test case writing is not fully completed, Its in progress .


Using Spring -Profiling for any variables that is static , but changes env to env.
Ex- Logging level is set to INFO at UAT but ERROR at prod properties/yml