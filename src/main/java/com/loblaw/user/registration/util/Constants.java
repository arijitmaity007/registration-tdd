package com.loblaw.user.registration.util;

import org.springframework.context.annotation.Configuration;

/**
 * Constants class.
 */
@Configuration
public class Constants {
	
	public static final String OUTBOUND_QUEUE_DIRECT_EXCHANGE = "loblawUsecaseDirectExchange";



}
