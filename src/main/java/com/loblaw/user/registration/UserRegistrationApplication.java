package com.loblaw.user.registration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.loblaw.user.registration.entity.UserDetailsRepository;


@SpringBootApplication(scanBasePackages="com.loblaw")
@EntityScan(basePackageClasses=com.loblaw.user.registration.entity.UserDetails.class)
@EnableJpaRepositories( basePackageClasses = UserDetailsRepository.class )

public class UserRegistrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserRegistrationApplication.class, args);
	}
}
