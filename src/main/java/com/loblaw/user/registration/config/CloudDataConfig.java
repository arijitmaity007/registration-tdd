package com.loblaw.user.registration.config;

import javax.sql.DataSource;

import org.springframework.cloud.config.java.AbstractCloudConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/*
 * CloudDataConfig is a class which helps Spring-boot application connects to the Cloud-Data-Service on the fly using Spring-cloud-Data-Connectors.
 * It will auto-connect to any DB-service on PCF with dynamicity
*/

@Configuration
@Profile("cloud")
public class CloudDataConfig extends AbstractCloudConfig
{

	@Bean
	public DataSource dataSource() {
	    return connectionFactory().dataSource();
	}

}