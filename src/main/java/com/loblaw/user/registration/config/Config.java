package com.loblaw.user.registration.config;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.loblaw.user.registration.entity.UserDetails;
import com.loblaw.user.registration.sync.req_resp.objects.UserRegistrationInput;

@Configuration
public class Config
{

	@Autowired
	ModelMapper mapper;

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

	public UserDetails mapUserRegistrationInputToEntity(UserRegistrationInput request) {
		return mapper.map(request, UserDetails.class);
	}



}
