package com.loblaw.user.registration.handlers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.NonTransientDataAccessException;
import org.springframework.dao.RecoverableDataAccessException;
import org.springframework.dao.TransientDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.datasource.init.ScriptException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpServerErrorException;

import com.loblaw.user.registration.sync.req_resp.objects.UserRegistrationResponseOutput;
import com.loblaw.user.registration.util.CommonUtils;

//CustomExceptionHandler is an common Exception handling class , Its a centralized controller which catches all the exceptions and take necessery action on specific exceptions

@ControllerAdvice
public class CustomExceptionHandler {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	CommonUtils commonUtils;
	
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<UserRegistrationResponseOutput>  handleGenericExceptions(Exception ex) 
	{
		logger.error("Generic Exception occured, Message: ", ex);
		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
		UserRegistrationResponseOutput errorMessage = new UserRegistrationResponseOutput();
		
		errorMessage.setStatus("Failure");
		errorMessage.setResponseCode("INTERNAL_SERVER_ERROR");
		errorMessage.setTimestamp(commonUtils.getCurrentDateTime());
					
		com.loblaw.user.registration.sync.req_resp.objects.Errors errors = new com.loblaw.user.registration.sync.req_resp.objects.Errors();
		List<com.loblaw.user.registration.sync.req_resp.objects.Errors> errorList = new ArrayList<com.loblaw.user.registration.sync.req_resp.objects.Errors>();

		errors.setCode("500");
		errors.setMessage("Exception occurred - "+ex);
		errorList.add(errors);
		
		errorMessage.setErrors(errorList);
		
		return new ResponseEntity<UserRegistrationResponseOutput>(errorMessage, status);

	}
	
}