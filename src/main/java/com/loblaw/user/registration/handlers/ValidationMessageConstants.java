package com.loblaw.user.registration.handlers;


//ValidationMessageConstants is a class defines constants with respect to Validation message

public class ValidationMessageConstants {

	
	//Generic messages
	public final static String mandatory = "Mandatory Field";
	public final static String conditionalValidation = "Conditional Field Validation Error";
	public final static String dateTime = "Expected valid DateTime, format: MM/dd/yyyy hh:mm:ss";
	public final static String date = "Expected valid Date, format: MM/dd/yyyy";
	
	//Patterns
	public final static String dateTimePattern = "(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01])/((19|20)\\d\\d) ([2][0-3]|[0-1][0-9]|[1-9]):[0-5][0-9]:([0-5][0-9]|[6][0])$";
	public final static String datePattern = "(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01])/((19|20)\\d\\d)";
	
	//Error Code
	public final static String mandatoryValCode = "FV001";
	public final static String customValCode = "FV002";
	
}
