
package com.loblaw.user.registration.sync.req_resp.objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.Setter;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "correlationId",
    "status",
    "responseCode",
    "timestamp",
    "data",
    "errors"
})
public class RetrivalResponseOutput {

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("correlationId")
    private String correlationId;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("status")
    private String status;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("responseCode")
    private String responseCode;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("timestamp")
    private String timestamp;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("data")
    private Data data;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("errors")
    private List<Errors> errors = new ArrayList<Errors>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

  
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

 
    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(correlationId).append(status).append(responseCode).append(timestamp).append(data).append(errors).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof RetrivalResponseOutput) == false) {
            return false;
        }
        RetrivalResponseOutput rhs = ((RetrivalResponseOutput) other);
        return new EqualsBuilder().append(correlationId, rhs.correlationId).append(status, rhs.status).append(responseCode, rhs.responseCode).append(timestamp, rhs.timestamp).append(data, rhs.data).append(errors, rhs.errors).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
