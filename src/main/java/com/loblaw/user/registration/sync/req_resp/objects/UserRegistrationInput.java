
package com.loblaw.user.registration.sync.req_resp.objects;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.loblaw.user.registration.handlers.ValidationMessageConstants;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
	"correlationId",
	"firstName",
	"lastName",
	"address",
	"telephone",
	"email"
})
public class UserRegistrationInput {


	/**
	 * 
	 * (Required)
	 * 
	 */


	@JsonProperty("correlationId")
	@NotNull(message = ValidationMessageConstants.mandatory)
	@Length(min = 1, max = 50)
	private String correlationId;

	/**
	 * 
	 * (Required)
	 * 
	 */

	@JsonProperty("firstName")
	@NotNull(message = ValidationMessageConstants.mandatory)
	@Length(min = 1, max = 50)
	private String firstName;


	@JsonProperty("lastName")
	@NotNull(message = ValidationMessageConstants.mandatory)
	@Length(min = 1, max = 50)
	private String lastName;
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("address")
	@Length(min = 1, max = 100)
	private String address;
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("telephone")
	@NotNull(message = ValidationMessageConstants.mandatory)
	@Length(min = 1, max = 15)
	private String telephone;
	/**
	 * 
	 * (Required)
	 * 
	 */
	@JsonProperty("email")
	@NotNull(message = ValidationMessageConstants.mandatory)
	@Length(min = 1, max = 50)
	private String email;


	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();




	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(lastName).append(address).append(telephone).append(email).append(additionalProperties).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof UserRegistrationInput) == false) {
			return false;
		}
		UserRegistrationInput rhs = ((UserRegistrationInput) other);
		return new EqualsBuilder().append(lastName, rhs.lastName).append(address, rhs.address).append(telephone, rhs.telephone).append(email, rhs.email).append(additionalProperties, rhs.additionalProperties).isEquals();
	}

}
