package com.loblaw.user.registration.entity;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.loblaw.user.registration.entity.UserDetails;
import com.loblaw.user.registration.entity.UserDetailsRepository;

//UserRegistrationRepositoryService class handling the Repository service layer , performs the JPA CRUD operations on Repository object and annotated with @Service

@Service
public class UserRegistrationRepositoryService {

	private Logger logger = LoggerFactory.getLogger(getClass());
/*
	@Autowired
	private UserDetailsRepository assetDetailsRepository;


	public List<UserDetails> getUserDetailsForSpecificFirstName(String firstName){
		List<UserDetails> userDetailsList = new ArrayList<>();
		try {
			assetDetailsRepository.findByFirstName(firstName).forEach(userDetailsList::add);
		}
		catch (Exception e ) {
			logger.error("DB call failed to retrieve User Details, For firstName - "+firstName+" Exception is "+e);
			throw e;
		}
		return userDetailsList;
	}

	
	public UserDetails getUserDetailsForSpecificEmail(String email){
		UserDetails userDetails = new UserDetails();
		try {
			userDetails= assetDetailsRepository.find
		}
		catch (Exception e ) {
			logger.error("DB call failed to retrieve User Details, For email - "+email+" Exception is "+e);
			throw e;
		}
		return userDetails;
	}

	public UserDetails getAssetDetailsForSpecificCorrelationID(String correlationID){
		UserDetails assetDetails = new UserDetails();
		try {
			assetDetails= assetDetailsRepository.findByCorrelationId(correlationID);
		}
		catch (Exception e ) {
			logger.error("DB call failed to retrieve Asset Details, For correlationID - "+correlationID+" Exception is "+e);
			throw e;
		}
		return assetDetails;
	}

	public void uploadSingleUserDetails(UserDetails userDetails, String correlationID){

		try {
			if(null != userDetails) {
				assetDetailsRepository.save(userDetails);
			}
		}
		catch (Exception e ) {
			logger.error("DB call failed to Upload User Details, For correlationId - "+correlationID+ " Exception is "+e);
			throw e;
		}

	}*/
}
