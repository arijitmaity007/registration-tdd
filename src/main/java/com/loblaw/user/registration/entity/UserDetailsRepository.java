package com.loblaw.user.registration.entity;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

//UserDetailsRepository is the CrudRepository extended Interface , It makes UserDetails entity object CRUDRepository/JPARepository enabled

public interface UserDetailsRepository extends CrudRepository <UserDetails , String> {

	//Fetches all the users list for which First Name is = firstName
	  List<UserDetails> findByFirstName (String firstName);
	  
	//Fetches all the users list for which First Name is = firstName
	  UserDetails findByCorrelationId(String correlationId);


}
