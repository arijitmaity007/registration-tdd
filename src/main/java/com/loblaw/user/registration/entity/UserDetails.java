package com.loblaw.user.registration.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

//UserDetails is the @Entity class and it defines the table structure in DB, Table gets auto-created with the below definition.

@Getter
@Setter
@Entity
@Table(indexes = {@Index(name = "firstName",  columnList="firstName", unique = false),
        @Index(name = "lastName", columnList="lastName", unique = false),
		@Index(name = "correlationId", columnList="correlationId", unique = true)})
// Version 1 Of Entity
public class UserDetails {
	
	@Id
	protected String email ="";
    protected String firstName = "";
    
    protected String lastName = "";
    protected String address = "";
    protected String telephone = "";
    protected String correlationId = "";
    

}
