package com.loblaw.user.registration.service.navigator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

//UserRegistrationServiceNavigator class handling the service-layer and annotated with @Service


@Service
public class UserRegistrationServiceNavigator {

	private Logger logger = LoggerFactory.getLogger(getClass());


}
